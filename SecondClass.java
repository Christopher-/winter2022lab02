//Method that takes int inputs and returns additions relative to the method called in question
public class SecondClass {
	//Takes as input a value of type int and returns the value + 1 
	public static int addOne(int userVal) {
		return userVal+1;
	}
	//Takes as input a value of type int from user and returns the value + 2
	public int addTwo(int userVal) {
		return userVal+2;
	}
}