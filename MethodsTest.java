//Tests different methods and their uses
public class MethodsTest {
	public static void main(String[] args) {
		int x = 10;
		String s1 = "hello";
		String s2 = "goodbye";
		//Variable of SecondClass for public method, serves as object
		SecondClass sc = new SecondClass();
		System.out.println("This is value of x before calling method: " + x);
		//Runs the method for taking in no input
		methodNoINputNoReturn();
		System.out.println("This is value of x after calling method: " + x);
		//Takes as input any value of type int
		methodOneInputNoReturn(x+50);
		//Takes as input a int value and a double value
		methodTwoInputNoReturn(1,2.0);
		//Takes nothing as input, returns a value
		int z = methodNoINputNoReturnInt();
		System.out.println("The value of int z is: " + z);
		//Takes as input two ints, returns a type double 
		double varUserSqrt =sumSqaureRoot(6,3);
		System.out.println("User square root value is: " + varUserSqrt);
		System.out.println("String s1 length is: " + s1.length());
		System.out.println("String s2 length is: " + s2.length()); 
		//Prints from the SecondClass file running the method addOne, takes int as input
		System.out.println(SecondClass.addOne(50));
		//Prints from the SecondClass file running method addTwo, takes as input and runs variable of type SecondClass
		System.out.println(sc.addTwo(50));
	}
	//Stores new variable x of type int, declares value of x declared here
	public static void methodNoINputNoReturn() {
		int x = 50;
		System.out.println("I'm a method that takes no input and returns nothing");
		System.out.println("This is the value of x within the method: " + x);
	}
	//Takes as input an int value and then declares the value within method
	public static void methodOneInputNoReturn(int userVal) {
		System.out.println("Inside the method one input no return");
		System.out.println("User value is " + userVal + " inside the method");
	}
	//Takes as input a value of type int and a value of type double, declares them within method
	public static void methodTwoInputNoReturn(int myValInt, double myValDouble) {
		System.out.println("The value of the int is: " + myValInt + " the value of double is: " + myValDouble);
	}
	//Takes as input null, returns int value of 6
	public static int methodNoINputNoReturnInt() {
		return 6;
	}
	//Takes as input two integers, returns square root of sum
	public static double sumSqaureRoot(int userVal1,int userVal2) {
		int userSum = userVal1 + userVal2;
		return Math.sqrt(userSum);
	}
}