//Computes areas of a square and a rectangle
public class AreaComputations {
	//Method that takes as input an int value for square length and returns multiplication of times itself
	public static int areaSquare (int squareLength){
		return squareLength*squareLength;
	}
	//Method that takes as input two int values for rectangle width and length and returns multiplication of the two values.
	public int areaRectangle (int rectangleLength, int rectangleWidth) {
		return rectangleLength*rectangleWidth;
	}
}	