import java.util.Scanner;
//Takes user input and finds area of the user input for the square and rectangle
public class PartThree {
	//Takes as input the length for a square, the width and length for a rectangle from a user through scanner, calls 
	//areaComputations class for the multiplications.
	public static void main (String []args) {
		Scanner sc = new Scanner(System.in);
		//areaComputations is now declared as a variable for the public method
		AreaComputations sca = new AreaComputations();
		System.out.print("Enter a length for the square length (int): ");
		int squareLength = sc.nextInt();
		//Prints out the square area after the value is returned from call
		System.out.println("Your square area is: " + AreaComputations.areaSquare(squareLength));
		System.out.print("Enter a length for the Rectangle length (int): ");
		int rectangleLength = sc.nextInt(); 
		System.out.print("Enter a length for the Rectangle width (int): ");
		int rectangleWidth = sc.nextInt();
		//Prints out the rectangle area after the value is returned from call
		System.out.println("Your rectangle area is: " + sca.areaRectangle(rectangleLength,rectangleWidth));
	}
}	